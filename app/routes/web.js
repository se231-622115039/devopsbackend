module.exports = app => {
    const course = require("../controllers/courseController");
    app.get("/course",course.AllCourse);
    app.post('/course/:id',course.AddStudent);
};