const mysql = require("mysql");
const dbConfig = require("../config/dbConfig");
const connection = mysql.createConnection({ 
    host: dbConfig.HOST, 
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB
});

connection.connect(err=>{
    if(err) throw err;
    console.log("database connected");
});

module.exports = connection;