const express = require('express');
const bp = require('body-parser');

const app = express();

app.use(bp.json());

app.use(bp.urlencoded({extended:true}));

app.get("/",(req,res)=>{
    console.log("The web page ready");
});

require("./app/routes/web")(app);

app.listen(3000,()=>{
    console.log("Server running at port 3000.");
});